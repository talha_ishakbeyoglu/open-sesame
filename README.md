# Open Sesame

This was my senior project at University. It's a remote door control system using an Android app to remotely open/close doors and also view their statuses. It uses a Ruby on Rails server to take commands from the Android app using a REST API and also controls the doors by sending messages to a python script running on Raspberry Pi's which control the doors using their GPIO pins. It was a very important learning experience and taught me a lot about pretty much everything used in this project.

### Branches

The branching scheme is a bit confusing I know. Basically instead of making two separate repositories I decided to have the RoR and Android parts in the same repo but in separate branches, named server and android respectively.